%global __jar_repack 0

Name:		ovirt-engine-wildfly
Version:	19.1.0
Release:	2
Summary:	WildFly Application Server for oVirt Engine
Group:		Virtualization/Management
License:	Apache 2.0
URL:		http://wildfly.org
Source:		https://github.com/wildfly/wildfly/archive/refs/tags/wildfly-19.1.0.Final.zip

BuildRequires:	unzip
BuildRequires:	chrpath
BuildRequires:  binutils

# Prevent breaking packages that require e.g. libapr-1.so
AutoReqProv:	no

%description
WildFly Application Server for oVirt Engine.

%build
true

%install
mkdir -p %{buildroot}%{_datadir}
unzip -d %{buildroot}%{_datadir} %{SOURCE0}
mv %{buildroot}%{_datadir}/wildfly-%{version}.Final %{buildroot}%{_datadir}/%{name}

# Remove the shared objects that aren't suitable for the architecture
# that the package is being built for:
function remove_shared_objects () {
  find \
      %{buildroot}%{_datadir}/%{name}/modules/system/layers/base \
      -depth \
      -type d \
      -name "$1" \
      -exec rm -rf {} \;
}
remove_shared_objects solaris-x86_64
remove_shared_objects solaris-sparcv9
%ifarch x86_64
remove_shared_objects linux-i686
%endif
%ifarch %{ix86}
remove_shared_objects linux-x86_64
%endif

# Delete the "rpath" of the remaining shared objects:
# Strip the remaining shared objects
find \
    %{buildroot}%{_datadir}/%{name}/modules/system/layers/base \
    -type f \
    -name '*.so' \
    -exec chrpath --delete {} \; \
    -exec strip {} \;

%files
%{_datadir}/%{name}


%changelog
* Fri Apr 07 2023 wangdi <wangdi@kylinos.cn> - 19.1.0-2
- strip shared objects

* Mon Jun 27 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 19.1.0-1
- Init package
